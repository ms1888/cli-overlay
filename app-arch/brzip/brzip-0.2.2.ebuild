# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson

DESCRIPTION="A compression utility based on Brotli algorithm"
HOMEPAGE="https://github.com/matijaskala/brzip.parallel"
SRC_URI="https://gitlab.com/ms1888/brzip/-/archive/${PV}/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""
RESTRICT="mirror"

RDEPEND="
	app-arch/brotli:=
	app-crypt/libmd:=
	dev-libs/xxhash:="
DEPEND="${RDEPEND}"
BDEPEND="virtual/pkgconfig"
