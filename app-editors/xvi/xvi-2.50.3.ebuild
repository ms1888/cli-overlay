# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs

DESCRIPTION="A portable multi-file text editor"
HOMEPAGE="https://martinwguy.github.io/xvi/"
SRC_URI="https://martinwguy.github.io/xvi/download/${P}.tar.gz"

LICENSE="xvi"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="|| ( sys-libs/ncurses:= sys-libs/netbsd-curses:= )"
RDEPEND="${DEPEND}"
BDEPEND="virtual/pkgconfig"

src_prepare() {
	default

	tc-export CC PKG_CONFIG
	sed -i "s@-ltermcap@$("${PKG_CONFIG}" --libs ncurses)@" src/make*.??* || die
	sed -i "s@ -s @ @" Makefile || die
}

src_install() {
	default

	rmdir "${ED}"/usr/share/doc/xvi || die
}
