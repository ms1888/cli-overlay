# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake flag-o-matic toolchain-funcs

DESCRIPTION="Re-implementation of the classic 4BSD ex/vi"
HOMEPAGE="https://sites.google.com/a/bostic.com/keithbostic/vi"
COMMIT_ID="v${PV}"
DB1_COMMIT="ae130db9078d3b5509a5692820a636db35f74c8e"
SRC_URI="https://github.com/lichray/nvi2/archive/${COMMIT_ID}.tar.gz -> ${P}.tar.gz
	https://github.com/matijaskala/db1/archive/${DB1_COMMIT}.tar.gz -> db1-${DB1_COMMIT}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~alpha amd64 ~arm hppa ~mips ppc ppc64 sparc x86 ~x64-macos"
IUSE="unicode"

DEPEND="
	|| ( sys-libs/ncurses:= sys-libs/netbsd-curses:= )
	dev-libs/libbsd
	virtual/libiconv"
BDEPEND="virtual/pkgconfig"
IDEPEND="app-eselect/eselect-vi"
RDEPEND="${DEPEND}"

#S=${WORKDIR}/nvi2-${COMMIT_ID}
S=${WORKDIR}/nvi2-${PV}

src_configure() {
	append-ldflags "-L${WORKDIR}/db1-${DB1_COMMIT}"

	local mycmakeargs=(
		-DDB_INCLUDE_DIR="${WORKDIR}/db1-${DB1_COMMIT}/include"
		-DUSE_WIDECHAR="$(usex unicode)"
	)

	cmake_src_configure
}

src_compile() {
	emake -C "${WORKDIR}/db1-${DB1_COMMIT}" libdb1.a CC=$(tc-getCC)
	cmake_src_compile
}

src_install() {
	dobin "${BUILD_DIR}/nvi"
	dosym nvi /usr/bin/nex
	dosym nvi /usr/bin/nview
	newman man/vi.1 nvi.1
	dosym nvi.1 /usr/share/man/man1/nex.1
	dosym nvi.1 /usr/share/man/man1/nview.1
}

pkg_postinst() {
	eselect vi update --if-unset
}

pkg_postrm() {
	eselect vi update --if-unset
}
